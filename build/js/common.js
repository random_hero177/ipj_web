$(document).ready(function(){
	var portfolio = {
		config: {
			activeItemClass: 'portfolio__item--active',
			itemClass: '.portfolio__item',
			fullVideoItem: '.js-portfolio-full-video',
			fullVideo: '.js-full-video',
			videoClass: '.js-portfolio-video',
			owlCarousel: '.owl-carousel',
			closeIcon: '.js-close-icon'
		},
		actions: {
			// append again each item of column once scrolled to bottom
			animateItem: function(item){
				item.addClass(portfolio.config.activeItemClass);
			},
			fixHeight: function(){
				var items = $(portfolio.config.videoClass);
				items.each(function(){
					var curHeight = $(this).height();
					$(this).height(curHeight);
				})
				
			},
			// Play video on hover section
			showGifOnHover: function (){
				var items = $(portfolio.config.videoClass);
				items.each(function(){
					var curImg = $(this).find('img'),
						curSrc = curImg.attr('src');
					$(this).hover(
						function() {
							curImg.attr("src", curSrc.replace('.jpg', '.gif'));
						},
						function() {
							curImg.attr("src", curSrc);
						}                         
					);   
				})
			},
			// SHOW CONTACTS
			showContacts: function(){
				$('.js-contact-menu').click(function(e){
					e.preventDefault();
					$('.contacts').addClass('contacts--active');
				});
			},
			showFullVideo: function(id){
				$('.full-video').addClass('full-video--active');
				$('body').addClass('overflow');
				$('.nav-bar').addClass('nav-bar--nbg');
				owl.trigger('to.owl.carousel', [id, 500]);
				
			},
			hideSection: function(elClass){
				console.log(elClass);
				$('body').removeClass('overflow');
				$('.' + elClass).removeClass(elClass+'--active');
				$('.nav-bar').removeClass('nav-bar--nbg');
				if(elClass == 'full-video'){
					var figure = $('.'+elClass).find('.owl-item.active video')
					figure.get(0).pause(); 
				}
			},
			showVideoDescr: function(){
				
			},
			mobileMenu: function() {
				$('.menu-mobile__icons').click(function(){
					$('.menu-mobile__icon').each(function(el){
						$(this).toggle()
					});
					if($('.menu-mobile').hasClass('active')){
						$('.menu-mobile').removeClass('active')
					} else {
						$('.menu-mobile').addClass('active')
					}
				});
			}
		},
		init: function(){
			$(portfolio.config.itemClass).each(function(i, elem){
				setTimeout(function(){
					portfolio.actions.animateItem($(elem));
				}, i * 75)
			});
			//portfolio.actions.endlessColumns();
			
			portfolio.actions.fixHeight();
			portfolio.actions.showGifOnHover();
			portfolio.actions.showContacts();
			// showFullVideo listener
			// самый простой вариант в итоге, каждому видосу на js при ините задавать attr data-id(i)
			$(portfolio.config.fullVideoItem).click(function(){
				var id = $(this).data('id');
				portfolio.actions.showFullVideo(parseInt(id));
			});
			$(portfolio.config.closeIcon).click(function(e){
				var parentEl = $(this).parent().attr('class'),
					parentElClass = parentEl.split(' ');
				portfolio.actions.hideSection(parentElClass[0]);
			})
			$('.full-video__descr').click(function(e){
				var elem = $(this).find('.full-video__descr-full');
				console.log(elem);
				elem.addClass('full-video__descr-full--active');
			});
			portfolio.actions.mobileMenu();
		}
	};
	portfolio.init();
	var owl = $('.owl-carousel');
		owl.owlCarousel({
		center: true,
		nav: true,
		items: 1,
		loop:true	
	});

	$('.owl-nav > div').click(function(){
		var figure = $('.owl-item').find('video');
			figure.each(function(i, el){
				$(el, this).get(0).pause();
			})
		
	})
});